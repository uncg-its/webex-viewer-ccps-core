<div class="card p-3">
    <h5 class="card-title">Google</h5>
    <div class="card-body text-center">
        <a href="{{ route('oauth', ['provider' => 'google']) }}" class="btn btn-primary btn-success" title="Login with Google">
            <i class="fab fa-google-plus mr-1"></i> Log in with Google
        </a>
    </div>
</div>
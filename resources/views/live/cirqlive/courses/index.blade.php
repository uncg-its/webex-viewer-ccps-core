@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive | Courses | Index'
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive-courses') !!}
    <h1>Live API: CirQlive - Courses</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('cirqlive.courses.all'),
            'fa' => 'mortar-board',
            'title' => 'All Courses'
        ])
    </div>
@endsection()
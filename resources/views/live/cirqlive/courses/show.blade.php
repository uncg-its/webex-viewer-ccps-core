@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive | Courses | ' . $id
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive-courses-show', $id) !!}
    <h1>Live API: CirQlive - Course Details</h1>

    @if($course)
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $course->name }}</h2>
                <h4>Connection info:</h4>
                @if($connection)
                    <p>
                        {{ $connection->platformType }} - <a href="{{ $connection->platformUrl }}" target="_blank">{{ $connection->platformUrl }}</a> (<a href="{{ route('cirqlive.connections.show', ['id' => $connection->meetsIdConnection]) }}">view details</a>)
                    </p>
                @else
                    <p>Not found.</p>
                @endif

                <h4>Other data:</h4>
                <p><strong>LTI ID Context</strong>: {{ $course->ltiIdContext }}</p>

                <ul>
                @foreach((array)$course->auxiliary as $setting => $value)
                    <li>{{ $setting }}: {{ $value ? 'true' : 'false' }}</li>
                @endforeach
                </ul>
                <hr>
                <h4>Debug:</h4>
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#raw" aria-expanded="false" aria-controls="raw">
                    Raw Course Info from API
                </a>
            </div>
        </div>
        <div class="collapse" id="raw">
            <pre>{{ print_r($course) }}</pre>
        </div>
    @else
        <p>Error: course with ID {{ $id }} does not exist.</p>
    @endif
@endsection()
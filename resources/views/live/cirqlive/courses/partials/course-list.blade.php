{!! $courses->header() !!}
<table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>MEETS ID</th>
        <th>Course Name</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($courses as $id => $course)
        <tr>
            <td>{{ $id }}</td>
            <td>{{ $course->name }}</td>
            <td>
                <a href="{{ route('cirqlive.courses.show', ['id' => $id]) }}" class="btn btn-primary">Course Details</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
    @if(isset($data))
        {!! $courses->appends($data)->render() !!}
    @else
        {!! $courses->render() !!}
    @endif
</div>
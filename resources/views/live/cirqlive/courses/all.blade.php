@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive | Courses | All Courses'
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive-courses-all') !!}
    <h1>Live API: CirQlive - All Courses</h1>
    @if(count($courses) > 0)
        @include('live.cirqlive.courses.partials.course-list', [
            'courses' => $courses
        ])
    @else
        <p>No courses to display.</p>
    @endif
@endsection()
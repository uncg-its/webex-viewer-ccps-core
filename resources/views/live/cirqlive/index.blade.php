@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive'
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive') !!}
    <h2>CirQlive (MEETS) API tools</h2>
    <p>This section deals with the MEETS service (provided by CirQlive), which requires a connection to an LMS</p>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('cirqlive.connections'),
            'fa' => 'plug',
            'title' => 'Connections'
        ])
        @include('components.panel-nav', [
            'url' => route('cirqlive.courses'),
            'fa' => 'mortar-board',
            'title' => 'Courses'
        ])
        {{--
        @include('components.panel-nav', [
            'url' => route('cirqlive.conferences'),
            'fa' => 'calendar',
            'title' => 'Conferences'
        ])
        @include('components.panel-nav', [
            'url' => route('cirqlive.recordings'),
            'fa' => 'film',
            'title' => 'Recordings'
        ])
        @include('components.panel-nav', [
            'url' => route('cirqlive.attendance'),
            'fa' => 'users',
            'title' => 'Attendance'
        ])
        --}}
    </div>
@endsection()
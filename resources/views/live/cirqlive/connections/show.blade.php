@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive | Connections | ' . $id
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive-connections-show', $id) !!}
    <h1>Live API: CirQlive - Connection Details</h1>

    @if($connection)
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $connection->name }}</h2>
                <h4>Platform:</h4>
                <p>
                    {{ $connection->platformType }} - <a href="{{ $connection->platformUrl }}" target="_blank">{{ $connection->platformUrl }}</a>
                </p>
                <h4>Configuration:</h4>
                <ul>
                @foreach((array)$connection->settings as $setting => $value)
                    <li>{{ $setting }}: {{ $value ? 'true' : 'false' }}</li>
                @endforeach
                </ul>
                <hr>
                <h4>Debug:</h4>
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#raw" aria-expanded="false" aria-controls="raw">
                    Raw Connection Info from API
                </a>
            </div>
        </div>
        <div class="collapse" id="raw">
            <pre>{{ print_r($connection) }}</pre>
        </div>
    @else
        <p>Error: connection with ID {{ $id }} does not exist.</p>
    @endif
@endsection()
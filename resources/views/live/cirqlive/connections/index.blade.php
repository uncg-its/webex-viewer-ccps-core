@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive | Connections | Index'
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive-connections') !!}
    <h1>Live API: CirQlive - Connections</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('cirqlive.connections.all'),
            'fa' => 'plug',
            'title' => 'All Connections'
        ])
    </div>
@endsection()
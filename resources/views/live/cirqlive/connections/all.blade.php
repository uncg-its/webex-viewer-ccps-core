@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | CirQlive | Connections | All Connections'
])

@section('content')
    {!! Breadcrumbs::render('live-cirqlive-connections-all') !!}
    <h1>Live API: CirQlive - All Connections</h1>
    @if(count($connections) > 0)
        {!! $connections->header() !!}
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>URL</th>
                    <th>Type</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
        @foreach($connections as $id => $connection)
                <tr>
                    <td>{{ $id }}</td>
                    <td>{{ $connection->name }}</td>
                    <td>{{ $connection->platformUrl}}</td>
                    <td>{{ $connection->platformType }}</td>
                    <td>
                        <a href="{{ route('cirqlive.connections.show', ['id' => $id]) }}" class="btn btn-primary">Connection Details</a>
                    </td>
                </tr>
        @endforeach
            </tbody>
        </table>
        <div class="text-center">
            @if(isset($data))
                {!! $connections->appends($data)->render() !!}
            @else
                {!! $connections->render() !!}
            @endif
        </div>
    @else
        <p>No connections to display.</p>
    @endif
@endsection()
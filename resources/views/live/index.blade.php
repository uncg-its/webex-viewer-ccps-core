@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Index'
])

@section('content')
    {!! Breadcrumbs::render('live') !!}
    <h2>Live API tools</h2>
    <h4>Tools</h4>
    <div class="row">
        @permission('webex.view')
            @include('components.panel-nav', [
                'url' => route('webex'),
                'fa' => 'fas fa-globe',
                'title' => 'WebEx'
            ])
        @endpermission
        @permission('cirqlive.view')
            @include('components.panel-nav', [
                'url' => route('cirqlive'),
                'fa' => 'fas fa-plug',
                'title' => 'CirQlive (MEETS)'
            ])
        @endpermission
    </div>
    <hr>
    <h4>Settings & Information</h4>
    <div class="row">
        @permission('cache.*')
        @include('components.panel-nav', [
            'url' => route('cache'),
            'fa' => 'fas fa-database',
            'title' => 'API Cache'
        ])
        @endpermission
        @permission('webex.view|cirqlive.view')
        @include('components.panel-nav', [
            'url' => route('statistics'),
            'fa' => 'fas fa-chart-line',
            'title' => 'API Call Statistics'
        ])
        @endpermission
    </div>
@endsection()
<p><em>Showing session information from the past {{ $historyDays }} {{ str_plural('day', $historyDays) }}</em></p>

{!! $sessions->header() !!}
<table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Session Name</th>
        <th>Host Username</th>
        <th>Host Name</th>
        <th>Started</th>
        <th>Ended</th>
        <th>Participants</th>
        <th>Center</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($sessions as $confId => $session)
        <tr>
            <td>{{ $confId }}</td>
            <td>{{ $session['history:confName'] }}</td>
            <td>{{ $session['history:hostWebExID'] }}</td>
            <td>{{ isset($session['history:hostName']) ? $session['history:hostName'] : 'unavailable' }}</td>
            <td>{{ $session['center'] == 'meeting' ? $session['history:meetingStartTime'] : $session['history:sessionStartTime'] }}</td>
            <td>{{ $session['center'] == 'meeting' ? $session['history:meetingEndTime'] : $session['history:sessionEndTime'] }}</td>
            <td>{{ isset($session['history:totalParticipants']) ? $session['history:totalParticipants'] : 'unavailable' }}</td>
            <td>{{ ucwords($session['center']) }} Center</td>
            <td>
                <a href="{{ route('webex.sessions.show', ['id' => $confId]) }}" class="btn btn-primary">Session Details</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
    @if(isset($data))
        {!! $sessions->appends($data)->render() !!}
    @else
        {!! $sessions->render() !!}
    @endif
</div>
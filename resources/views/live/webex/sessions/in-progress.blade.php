@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Sessions | In Progress'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-sessions-in-progress') !!}
    <h1>Live API: Sessions In Progress</h1>
    @if(count($sessions) > 0)
        {!! $sessions->header("user") !!}
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>Session Key</th>
                <th>Session Name</th>
                <th>Host Username</th>
                <th>Scheduled Start</th>
                <th>Actual Start</th>
                <th>Center</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sessions as $sessionKey => $session)
                <tr>
                    <td>{{ $sessionKey }}</td>
                    <td>{{ $session['ep:sessionName'] }}</td>
                    <td>{{ $session['ep:hostWebExID'] }}</td>
                    <td>{{ $session['ep:startTime'] }}</td>
                    <td>{{ $session['ep:actualStartTime'] }}</td>
                    <td>{{ str_replace('Center', ' Center', $session['center']) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            {!! $sessions->render() !!}
        </div>
    @else
        <p>No sessions to display.</p>
    @endif
@endsection()
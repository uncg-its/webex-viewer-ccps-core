@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Sessions | Scheduled'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-sessions-scheduled') !!}
    <h1>Live API: Scheduled Sessions</h1>
    @if(count($sessions) > 0)
        {!! $sessions->header() !!}
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>Session Key</th>
                <th>Center</th>
                <th>Session Name</th>
                <th>Host Username</th>
                <th>Start Date</th>
                <th>Time Zone</th>
                <th>Duration</th>
                {{--<th>Actions</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($sessions as $sessionKey => $session)
                <tr>
                    <td>{{ $sessionKey }}</td>
                    <td>{{ ucwords($session['center']) }} Center</td>
                    <td>{{ $session['sessionName'] }}</td>
                    <td>{{ $session['hostWebExID'] }}</td>
                    <td>{{ $session['startDate'] }}</td>
                    <td>{{ $session['timeZone'] ?? '' }}</td>
                    <td>{{ $session['duration'] ?? '' }}</td>
                    {{--<td>--}}
                    {{--<a href="{{ route('webex.sessions.showScheduled', ['id' => $sessionKey]) }}"--}}
                    {{--class="btn btn-primary">Session--}}
                    {{--Details</a>--}}
                    {{--</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center">
            @if(isset($data))
                {!! $sessions->appends($data)->render() !!}
            @else
                {!! $sessions->render() !!}
            @endif

            @else
                <p>No sessions to display.</p>
    @endif
@endsection()
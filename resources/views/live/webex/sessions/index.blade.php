@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Sessions | Index'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-sessions') !!}
    <h1>Live API: Sessions</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('webex.sessions.scheduled'),
            'fa' => 'fas fa-calendar',
            'title' => 'All Scheduled Sessions'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.sessions.all'),
            'fa' => 'fas fa-hourglass',
            'title' => 'All Past Sessions'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.sessions.inprogress'),
            'fa' => 'fas fa-clock',
            'title' => 'Sessions In Progress'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.sessions.search'),
            'fa' => 'fas fa-search',
            'title' => 'Search Past Sessions'
        ])
    </div>
@endsection()
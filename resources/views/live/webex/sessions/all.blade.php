@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Sessions | All Sessions'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-sessions-all') !!}
    <h1>Live API: All Sessions</h1>
    @if(count($sessions) > 0)
        @include('live.webex.sessions.partials.session-list', [
            'sessions' => $sessions,
            'historyDays' => $historyDays
        ])
    @else
        <p>No sessions to display.</p>
    @endif
@endsection()
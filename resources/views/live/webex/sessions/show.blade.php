@extends('layouts.wrapper', [ 'pageTitle' => 'Live API | Sessions | ' . $confId ]) 
@section('content') {!! Breadcrumbs::render('live-webex-sessions-show',
$confId, $center) !!}
<h1>Live API: Session Details</h1>
@if(!$session)
<div class="alert alert-danger"><i class="fas fa-exclamation-triangle"></i> Session with specified ID was not found.</div>
@else
<h4>Session Information</h4>
<p><strong>Conference ID:</strong> {{ $confId }}</p>
<p><strong>Session Name:</strong> {{ $session['history:confName'] }}</p>
<p><strong>Hosted by:</strong> <a href="{{ route('webex.users.show', ['id' => $session['history:hostWebExID']]) }}" target="_blank"
        title="View user information">{{ $center != 'training' ? $session['history:hostName'] : '' }} ({{ $session['history:hostWebExID'] }})</a>    <i class="fas fa-external-link-square-alt"></i></p>
<p><strong>Recordings:</strong> <a href="{{ route('webex.recordings.search', ['criterion' => 'confID', 'operand' => 'exact', 'search' => $confId]) }}"
        class="btn btn-primary" target="_blank">Look for session recordings <i class="fas fa-external-link-square-alt"></i></a></p>

<hr>
<div class="row">
    <div class="col-md-6">
        <h4>Date / Time information</h4>
        <p><strong>Start time:</strong> {{ $center == 'meeting' ? $session['history:meetingStartTime'] : $session['history:sessionStartTime']
            }}</p>
        <p><strong>End time:</strong> {{ $center == 'meeting' ? $session['history:meetingEndTime'] : $session['history:sessionEndTime']
            }}</p>
        <p><strong>Time Zone:</strong> {{ $session['history:timeZoneWithDST'] }}</p>
        <p><strong>Duration:</strong> {{ $session['history:duration'] }} min.</p>
    </div>
    <div class="col-md-6">
        <h4>Participant Information</h4>
        <p><strong>Total participants:</strong> {{ $session['history:totalParticipants'] }}</p>
        <p><strong>People minutes:</strong> {{ $session['history:totalPeopleMinutes'] }}</p>
        <p><strong>VoIP participants:</strong> {{ $session['history:totalParticipantsVoip'] }}</p>
        <p><strong>Call-in participants:</strong> {{ $session['history:totalParticipantsCallIn'] }}</p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
        <h4>Debug:</h4>
        <a class="btn btn-primary" role="button" data-toggle="collapse" href="#raw" aria-expanded="false" aria-controls="raw">
                    Raw Session Info from API
                </a>
    </div>
</div>

<div class="collapse" id="raw">
    <pre>{{ print_r($session) }}</pre>
</div>
@endif
@endsection
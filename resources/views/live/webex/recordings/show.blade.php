@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Recordings | ' . $id
])

@section('content')
    {!! Breadcrumbs::render('live-webex-recordings-show', $id) !!}
    <h1>Live API: Recording Details</h1>
    @if(!$recording)
        <div class="alert alert-danger"><i class="fas fa-exclamation-triangle"></i> Recording with specified ID was not found.</div>
    @else
        <h4>Recording Information</h4>
        <p><strong>ID:</strong> {{ $id }}</p>
        <p><strong>Recording Name:</strong> {{ $recording['ep:name'] }}</p>
        <p><strong>Hosted by:</strong> <a href="{{ route('webex.users.show', ['id' => $recording['ep:hostWebExID']]) }}" target="_blank" title="View user information">{{ $recording['ep:hostWebExID'] }}</a> <i class="fas fa-external-link-square-alt"></i></p>
        <p><strong>Parent Session:</strong> <a href="{{ route('webex.sessions.show', ['id' => $recording['ep:confID']]) }}" class="btn btn-primary" target="_blank">Session Details <i class="fas fa-external-link-square-alt"></i></a></p>

        <hr>
        <div class="row">
            <div class="col-md-6">
                <h4>Recording properties</h4>
                <p><strong>Create time:</strong> {{ $recording['ep:createTime'] }}</p>
                <p><strong>Duration:</strong> {{ \Uncgits\WebexApi\Helpers::secondsToTime($recording['ep:duration']) }}</p>
                <p><strong>Size (MB):</strong> {{ $recording['ep:size'] }}</p>
            </div>
            <div class="col-md-6">
                <h4>Stream / Download</h4>
                <a href="{{ $recording['ep:streamURL'] }}" target="_blank" class="btn btn-warning"><i class="fas fa-podcast"></i> Stream</a>
                <a href="{{ $recording['ep:fileURL'] }}" target="_blank" class="btn btn-success"><i class="fas fa-download"></i> Download</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
                <h4>Debug:</h4>
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#raw" aria-expanded="false"
                   aria-controls="raw">
                    Raw Recording Info from API
                </a>
            </div>
        </div>

        <div class="collapse" id="raw">
            <pre>{{ print_r($recording) }}</pre>
        </div>
    @endif
@endsection()
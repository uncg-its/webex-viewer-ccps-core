@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Recordings | Index'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-recordings') !!}
    <h1>Live API: Sessions</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('webex.recordings.all'),
            'fa' => 'fas fa-film',
            'title' => 'All Recordings'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.recordings.search'),
            'fa' => 'fas fa-search',
            'title' => 'Search Recordings'
        ])
    </div>
@endsection()
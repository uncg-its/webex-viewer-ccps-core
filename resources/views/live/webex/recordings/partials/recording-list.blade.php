<p><em>Showing recording information from the past {{ $historyDays }} {{ str_plural('day', $historyDays) }}</em></p>

{!! $recordings->header() !!}
<table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>Recording ID</th>
        <th>Name</th>
        <th>Host Username</th>
        <th>Created</th>
        <th>Size (MB)</th>
        <th>Duration (hh:mm:ss)</th>
        <th>Center</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($recordings as $recordingId => $recording)
        <tr>
            <td>{{ $recordingId }}</td>
            <td>{{ $recording['ep:name'] }}</td>
            <td>{{ $recording['ep:hostWebExID'] }}</td>
            <td>{{ $recording['ep:createTime'] }}</td>
            <td>{{ round($recording['ep:size'], 2) }}</td>
            <td>{{ \Uncgits\WebexApi\Helpers::secondsToTime($recording['ep:duration']) }}</td>
            <td>{{ str_replace('Center', ' Center', $recording['ep:serviceType']) }}</td>
            <td>
                <a href="{{ route('webex.recordings.show', ['id' => $recordingId]) }}" class="btn btn-danger"><i class="fas fa-film"></i> Recording Details</a>
                <a href="{{ route('webex.sessions.show' , ['id' => $recording['ep:confID']]) }}" class="btn btn-primary"><i class="fas fa-ellipsis-h"></i> Session Details</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
    @if(isset($data))
        {!! $recordings->appends($data)->render() !!}
    @else
        {!! $recordings->render() !!}
    @endif
</div>
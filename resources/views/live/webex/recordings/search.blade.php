@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Recordings | Search Recordings'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-recordings-search') !!}
    <h1>Live API: Search Recordings</h1>
    <p><em>Currently searching within the past {{ $historyDays }} {{ str_plural('day', $historyDays) }}</em></p>

    @if(isset($results))
        <h3>Search results for '{{ $criterion }} {{ $operand }} {{ $search }}':</h3>
        @if(count($results) == 0)
            <p>No match.</p>
        @else
            @include('live.webex.recordings.partials.recording-list', [
                'recordings' => $results,
                'data' => compact(['criterion', 'operand', 'search'])
            ])
        @endif

        <hr>
    @endif

    <h3>Search</h3>
    <form action="{{ route('webex.recordings.search') }}" method="get">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="criterion">Criterion:</label>
                    <select id="criterion" class="form-control" name="criterion">
                        <option value="recordingID" selected>Recording ID</option>
                        <option value="hostWebExID">Host Webex ID (username)</option>
                        <option value="confID">Session ID</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="operand">Operand:</label>
                    <select id="operand" class="form-control" name="operand">
                        <option value="contains" selected>contains</option>
                        <option value="begins">begins with</option>
                        <option value="ends">ends with</option>
                        <option value="exact">matches exactly</option>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="search">Search:</label>
                    <input type="text" class="form-control" id="search" name="search" placeholder="Search">
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Search</button>

    </form>

@endsection()
@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Recordings | All Recordings'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-recordings-all') !!}
    <h1>Live API: All Recordings</h1>
    @if(count($recordings) > 0)
        @include('live.webex.recordings.partials.recording-list', [
            'recordings' => $recordings,
            'historyDays' => $historyDays
        ])
    @else
        <p>No recordings to display.</p>
    @endif
@endsection()
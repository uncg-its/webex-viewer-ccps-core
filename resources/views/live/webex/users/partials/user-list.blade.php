{!! $users->header() !!}
<table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>Webex ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $webExId => $user)
        <tr>
            <td>{{ $webExId }}</td>
            <td>{{ $user['use:firstName'] }}</td>
            <td>{{ $user['use:lastName'] }}</td>
            <td>{{ $user['use:email'] }}</td>
            <td>
                <a href="{{ route('webex.users.show', ['id' => $webExId]) }}" class="btn btn-primary">User Details</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
    @if(isset($data))
        {!! $users->appends($data)->render() !!}
    @else
        {!! $users->render() !!}
    @endif
</div>
@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Users | Search Users'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-users-search') !!}
    <h1>Live API: Search Users</h1>
    @if(isset($results))
        <h3>Search results for {{ $status }} users matching '{{ $criterion }} {{ $operand }} {{ $search }}':</h3>
        @if(count($results) == 0)
            <p>No match.</p>
        @else
            @include('live.webex.users.partials.user-list', [
                'users' => $results,
                'data' => compact(['status', 'criterion', 'operand', 'search'])
            ])
        @endif

        <hr>
    @endif

    <h3>Search</h3>
    <form action="{{ route('webex.users.search') }}" method="get">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="status">Users with active status:</label>
                    <select id="status" class="form-control" name="status">
                        <option value="any" selected>Any</option>
                        <option value="active">Active only</option>
                        <option value="inactive">Inactive only</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="criterion">Criterion:</label>
                    <select id="criterion" class="form-control" name="criterion">
                        <option value="webExId" selected>Webex ID (username)</option>
                        <option value="firstName">First Name</option>
                        <option value="lastName">Last Name</option>
                        <option value="email">Email Address</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="operand">Operand:</label>
                    <select id="operand" class="form-control" name="operand">
                        <option value="contains" selected>contains</option>
                        <option value="begins">begins with</option>
                        <option value="ends">ends with</option>
                        <option value="exact">matches exactly</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="search">Search:</label>
                    <input type="text" class="form-control" id="search" name="search" placeholder="Search">
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Search</button>

    </form>

@endsection()
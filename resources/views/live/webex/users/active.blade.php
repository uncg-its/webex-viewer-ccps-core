@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Users | Active Users'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-users-active') !!}
    <h1>Live API: Active Users</h1>
    @if(count($users) > 0)
        @include('live.webex.users.partials.user-list', [
            'users' => $users
        ])
    @else
        <p>No users to display.</p>
    @endif
@endsection()
@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Users | Index'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-users') !!}
    <h1>Live API: Users</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('webex.users.all'),
            'fa' => 'fas fa-users',
            'title' => 'All Users'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.users.active'),
            'fa' => 'fas fa-user-circle',
            'title' => 'Active Users'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.users.search'),
            'fa' => 'fas fa-search',
            'title' => 'Search Users'
        ])
    </div>
@endsection()
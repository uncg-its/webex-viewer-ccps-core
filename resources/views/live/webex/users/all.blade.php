@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Users | All Users'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-users-all') !!}
    <h1>Live API: All Users</h1>
    @if(count($users) > 0)
        @include('live.webex.users.partials.user-list', [
            'users' => $users
        ])
    @else
        <p>No users to display.</p>
    @endif
@endsection()
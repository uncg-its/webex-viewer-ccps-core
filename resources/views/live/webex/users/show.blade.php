@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Users | ' . $webExUser['use:webExId']
])

@section('content')
    {!! Breadcrumbs::render('live-webex-users-show', $webExUser['use:webExId']) !!}
    <h1>Live API: User details</h1>
    <div class="row">
        <div class="col-md-3">
            <img class="img-responsive img-thumbnail" src="{{ $webExUser['use:avatar']['use:url'] }}">
        </div>
        <div class="col-md-9">
            <h2>{{ $webExUser['use:firstName'] }} {{ $webExUser['use:lastName'] }} ({{ $webExUser['use:webExId'] }})</h2>
            @if($webExUser['use:privilege']['use:siteAdmin'] == 'true')
                <p class="text-danger"><strong><i class="fas fa-star"></i> Site Admin</strong></p>
            @endif
            <p><strong>Email:</strong> {{ $webExUser['use:email'] }}</p>
            @if(isset($webExUser['use:personalMeetingRoom']['use:personalMeetingRoomURL']))
                <p><a class="btn btn-primary" href="{{ $webExUser['use:personalMeetingRoom']['use:personalMeetingRoomURL'] }}"><i class="fas fa-user"></i> Personal Meeting Room</a></p>
            @else
                <p><em>No Personal Meeting Room configured</em></p>
            @endif
            <hr>
            <h4>Center privileges:</h4>
            <div class="row">
                @foreach($siteKeys as $index => $displayName)
                    <div class="col-md-3 text-center">
                        @if($webExUser['use:supportedServices'][$index] == 'true')
                            <p class="bg-success"><i class="fas fa-check"></i> {{ $displayName }}</p>
                        @else
                            <p class="bg-danger"><i class="fas fa-times"></i> {{ $displayName }}</p>
                        @endif
                    </div>
                @endforeach
            </div>
            <hr>
            <h4>Related data:</h4>
            <div class="row">
                <div class="col-md-6">
                    <a href="{{ route('webex.sessions.search', ['criterion' => 'hostWebExID', 'operand' => 'exact', 'search' => $webExUser['use:webExId']]) }}" class="btn btn-warning"><i class="fas fa-search"></i> Search for sessions by this user</a>
                </div>
                <div class="col-md-6">
                    <a href="{{ route('webex.recordings.search', ['criterion' => 'hostWebExID', 'operand' => 'exact', 'search' => $webExUser['use:webExId']]) }}" class="btn btn-danger"><i class="fas fa-search"></i> Search for recordings by this user</a>
                </div>
            </div>
            <hr>
            <h4>Account information:</h4>
            <div class="row">
                <div class="col-md-6"><p><strong>Created:</strong> {{ $webExUser['use:registrationDate'] }}</p>
                    <p><strong>Expires:</strong> {{ $webExUser['use:expirationDate'] }}</p>
                    <p><strong>Times visiting site:</strong> {{ $webExUser['use:visitCount'] }}</p>

                    <p><strong>Time Zone:</strong> {{ $webExUser['use:timeZoneWithDST'] }}</p></div>
                <div class="col-md-6">
                    <p><strong> Phone number: </strong> {{ $webExUser['use:phones']['com:phone'] ?? "unlisted" }} </p>
                    <p><strong> Mobile number: </strong> {{ $webExUser['use:phones']['com:mobilePhone'] ?? "unlisted"}} </p>
                </div>
            </div>

            <hr>
            <h4>Debug:</h4>
            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#raw" aria-expanded="false" aria-controls="raw">
                Raw User Info from API
            </a>
        </div>
    </div>

    <div class="collapse" id="raw">
        <pre>{{ print_r($webExUser) }}</pre>
    </div>
@endsection()
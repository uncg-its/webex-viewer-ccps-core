@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Webex'
])

@section('content')
    {!! Breadcrumbs::render('live-webex') !!}
    <h2>Webex API tools</h2>
    <p>The tools on this page use the Webex API to look up data in real time. Note that there is a delay with the Webex API; new items (e.g. recently concluded sessions) often take time to "post" and become available via API, sometimes up to 24 hours.</p>
    <hr>
    <h4>Tools</h4>
    <div class="row">
        @permission('webex.view')
        @include('components.panel-nav', [
            'url' => route('webex.site'),
            'fa' => 'fas fa-window-maximize',
            'title' => 'Site'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.users'),
            'fa' => 'fas fa-users',
            'title' => 'Users'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.sessions'),
            'fa' => 'fas fa-hourglass',
            'title' => 'Sessions'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.recordings'),
            'fa' => 'fas fa-film',
            'title' => 'Recordings'
        ])
        @endpermission
    </div>
@endsection()
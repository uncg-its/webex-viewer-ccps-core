@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Site | WebEx Site Info'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-site-info') !!}
    <h1>Live API: WebEx Site Info</h1>
    <pre>{{ var_dump($site) }}</pre>
@endsection()
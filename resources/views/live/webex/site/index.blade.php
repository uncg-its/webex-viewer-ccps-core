@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Site | Index'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-site') !!}
    <h1>Live API: Site</h1>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('webex.site.info'),
            'fa' => 'fas fa-info-circle',
            'title' => 'WebEx Site Info'
        ])
        @include('components.panel-nav', [
            'url' => route('webex.site.apiversion'),
            'fa' => 'fas fa-code',
            'title' => 'XML API Version'
        ])
    </div>
@endsection()
@extends('layouts.wrapper', [
    'pageTitle' => 'Live API | Site | XML API Info'
])

@section('content')
    {!! Breadcrumbs::render('live-webex-site-xml-api') !!}
    <h1>Live API: XML API Info</h1>
    <pre>{{ var_dump($api) }}</pre>
@endsection()
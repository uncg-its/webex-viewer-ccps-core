{!! Form::open()->patch() !!}
{!! Form::fieldsetOpen('API Config options') !!}
<div class="form-group">
    <label for="default_environment">Active Webex API Environment:</label>
    <select class="form-control" name="environment" id="environment">
        @foreach($environments as $key => $name)
            <option value="{{ $key }}"
                    @if($dbConfig->has('environment') && $dbConfig['environment'] == $key)
                    selected
                    @endif
            >{{ $name }}</option>
        @endforeach
    </select>
</div>
{!! Form::fieldsetClose() !!}
<br>
{!! Form::submit('Update config') !!}
{!! Form::close() !!}
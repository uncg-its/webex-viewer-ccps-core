<div class="card">
    <div class="card-header">
        <h5>Audit History</h5>
    </div>
    <div class="card-body">
        @if(count($audits) > 0)
            <p><strong>Recent changes</strong> (showing last {{ config('ccps.audits_to_show') }}):</p>
            <ul>
                @foreach($audits as $audit)
                    @if($audit->event == 'updated')
                        <li>
                            <a href="#audit{{ $audit->id }}" data-toggle="modal" title="Show audit">updated</a> ({{ $audit->created_at }})
                            <div class="modal fade" id="audit{{ $audit->id }}" tabindex="-1" role="dialog" aria-labelledby="audit{{ $audit->id }}-label" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="audit{{ $audit->id }}-label">Audit #{{ $audit->id }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p><strong>Old values</strong>:</p>
                                            <small>
                                                <pre>{{ var_dump($audit->old_values) }}</pre>
                                            </small>
                                            <p><strong>New values</strong>:</p>
                                            <small>
                                                <pre>{{ var_dump($audit->new_values) }}</pre>
                                            </small>
                                            <p><strong>IP Address</strong>: <a href="http://www.ip-tracker.org/locator/ip-lookup.php?ip={{ $audit->ip_address }}" target="_blank">{{ $audit->ip_address }}</a></p>
                                            <p><strong>User Agent</strong>: {{ $audit->user_agent }}</p>
                                            <p><strong>Date / Time</strong>: {{ $audit->created_at }}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @else
                        <li>{{ $audit->event }} ({{ $audit->created_at }})</li>
                    @endif
                @endforeach
            </ul>

            <p><a href="{{ route($modelType . '.audits', ['id' => $model->id]) }}">Full audit history</a></p>

        @else
            <p>No audit history to show.</p>
        @endif
    </div>
</div>
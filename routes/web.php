<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CcpsCore\HomeController@index')->name('home');
Route::get('home', 'CcpsCore\HomeController@index');

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');

// -----------------------
// Webex Viewer app routes
// -----------------------

Route::group(['prefix' => 'live', 'middleware' => ['auth']], function () {
    Route::get('/', 'LiveController@index')->name('live');

    Route::group(['prefix' => 'webex'], function () {
        Route::get('/', 'WebexController@index')->name('webex');

        Route::group(['prefix' => 'site'], function () {
            Route::get('/', 'WebexController@siteIndex')->name('webex.site');
            Route::get('/info', 'WebexController@siteInfo')->name('webex.site.info');
            Route::get('/xml-api', 'WebexController@siteXmlApiVersion')->name('webex.site.apiversion');
        });


        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'WebexController@usersIndex')->name('webex.users');
            Route::get('/all', 'WebexController@usersAll')->name('webex.users.all');
            Route::get('/active', 'WebexController@usersActive')->name('webex.users.active');
            Route::get('/search', 'WebexController@usersSearch')->name('webex.users.search');
            Route::get('/id/{id}', 'WebexController@usersShow')->name('webex.users.show');
        });

        Route::group(['prefix' => 'sessions'], function () {
            Route::get('/', 'WebexController@sessionsIndex')->name('webex.sessions');
            Route::get('/all', 'WebexController@sessionsAll')->name('webex.sessions.all');
            Route::get('/scheduled', 'WebexController@sessionsScheduled')->name('webex.sessions.scheduled');
            Route::get('/scheduled/{id}',
                'WebexController@sessionsScheduledShow')->name('webex.sessions.showScheduled');
            Route::get('/in-progress', 'WebexController@sessionsInProgress')->name('webex.sessions.inprogress');
            Route::get('/search', 'WebexController@sessionsSearch')->name('webex.sessions.search');
            Route::get('/id/{id}', 'WebexController@sessionsShow')->name('webex.sessions.show');
        });

        Route::group(['prefix' => 'recordings'], function () {
            Route::get('/', 'WebexController@recordingsIndex')->name('webex.recordings');
            Route::get('/all', 'WebexController@recordingsAll')->name('webex.recordings.all');
            Route::get('/search', 'WebexController@recordingsSearch')->name('webex.recordings.search');
            Route::get('/id/{id}', 'WebexController@recordingsShow')->name('webex.recordings.show');
        });
    });

    Route::group(['prefix' => 'cirqlive'], function () {
        Route::get('/', 'CirqliveController@index')->name('cirqlive');

        Route::group(['prefix' => 'connections'], function () {
            Route::get('/', 'CirqliveController@connectionsIndex')->name('cirqlive.connections');
            Route::get('/all', 'CirqliveController@connectionsAll')->name('cirqlive.connections.all');
            Route::get('/id/{id}', 'CirqliveController@connectionsShow')->name('cirqlive.connections.show');
        });

        Route::group(['prefix' => 'courses'], function () {
            Route::get('/', 'CirqliveController@coursesIndex')->name('cirqlive.courses');
            Route::get('/all', 'CirqliveController@coursesAll')->name('cirqlive.courses.all');
            Route::get('/id/{id}', 'CirqliveController@coursesShow')->name('cirqlive.courses.show');
        });


    });

    Route::get('/statistics',
        '\Uncgits\CcpsApiLog\Http\Controllers\StatisticsController@index')->name('statistics')->middleware('permission:webex.view|cirqlive.view');
});
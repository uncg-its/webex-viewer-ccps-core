<?php

// LIVE API
Breadcrumbs::register('live', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Live API', route('live'));
});

// WEBEX
Breadcrumbs::register('live-webex', function ($breadcrumbs) {
    $breadcrumbs->parent('live');
    $breadcrumbs->push('Webex', route('webex'));
});

// SITE
Breadcrumbs::register('live-webex-site', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex');
    $breadcrumbs->push('Site', route('webex.site'));
});
Breadcrumbs::register('live-webex-site-info', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-site');
    $breadcrumbs->push('Site Info', route('webex.site.info'));
});
Breadcrumbs::register('live-webex-site-xml-api', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-site');
    $breadcrumbs->push('XML API info', route('webex.site.apiversion'));
});
// USERS
Breadcrumbs::register('live-webex-users', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex');
    $breadcrumbs->push('Users', route('webex.users'));
});
Breadcrumbs::register('live-webex-users-all', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-users');
    $breadcrumbs->push('All Users', route('webex.users.all'));
});
Breadcrumbs::register('live-webex-users-active', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-users');
    $breadcrumbs->push('Active Users', route('webex.users.active'));
});
Breadcrumbs::register('live-webex-users-search', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-users');
    $breadcrumbs->push('Search Users', route('webex.users.search'));
});
Breadcrumbs::register('live-webex-users-show', function ($breadcrumbs, $webExId) {
    $breadcrumbs->parent('live-webex-users');
    $breadcrumbs->push('User Info: ' . $webExId, route('webex.users.show', ['id' => $webExId]));
});
// SESSIONS
Breadcrumbs::register('live-webex-sessions', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex');
    $breadcrumbs->push('Sessions', route('webex.sessions'));
});
Breadcrumbs::register('live-webex-sessions-all', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-sessions');
    $breadcrumbs->push('All Past Sessions', route('webex.sessions.all'));
});
Breadcrumbs::register('live-webex-sessions-scheduled', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-sessions');
    $breadcrumbs->push('Scheduled Sessions', route('webex.sessions.scheduled'));
});
Breadcrumbs::register('live-webex-sessions-showScheduled', function ($breadcrumbs, $sessionKey) {
    $breadcrumbs->parent('live-webex-sessions');
    $breadcrumbs->push('Session Info: ' . $sessionKey, route('webex.sessions.scheduled'));
});
Breadcrumbs::register('live-webex-sessions-in-progress', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-sessions');
    $breadcrumbs->push('Sessions In Progress', route('webex.sessions.inprogress'));
});
Breadcrumbs::register('live-webex-sessions-search', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-sessions');
    $breadcrumbs->push('Search Past Sessions', route('webex.sessions.search'));
});
Breadcrumbs::register('live-webex-sessions-show', function ($breadcrumbs, $confId, $center) {
    $referrerPage = array_slice(explode('/', URL::previous()), -1)[0];

    if ($referrerPage != 'all' && $referrerPage != 'search') {
        $referrerPage = 'all';
    }

    $breadcrumbs->parent('live-webex-sessions-' . $referrerPage);
    $breadcrumbs->push('Session Info: ' . $confId,
        route('webex.sessions.show', ['id' => $confId, 'center' => $center]));
});
// RECORDINGS
Breadcrumbs::register('live-webex-recordings', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex');
    $breadcrumbs->push('Recordings', route('webex.recordings'));
});
Breadcrumbs::register('live-webex-recordings-all', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-recordings');
    $breadcrumbs->push('All Recordings', route('webex.recordings.all'));
});
Breadcrumbs::register('live-webex-recordings-search', function ($breadcrumbs) {
    $breadcrumbs->parent('live-webex-recordings');
    $breadcrumbs->push('Search Recordings', route('webex.recordings.search'));
});
Breadcrumbs::register('live-webex-recordings-show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('live-webex-recordings');
    $breadcrumbs->push('Recording Info: ' . $id, route('webex.recordings.show', ['id' => $id]));
});

// STATISTICS
Breadcrumbs::register('live-statistics', function ($breadcrumbs) {
    $breadcrumbs->parent('live');
    $breadcrumbs->push('API Statistics', route('statistics'));
});

// CIRQLIVE

Breadcrumbs::register('live-cirqlive', function ($breadcrumbs) {
    $breadcrumbs->parent('live');
    $breadcrumbs->push('CirQlive (MEETS)', route('cirqlive'));
});

// CONNECTIONS
Breadcrumbs::register('live-cirqlive-connections', function ($breadcrumbs) {
    $breadcrumbs->parent('live-cirqlive');
    $breadcrumbs->push('Connections', route('cirqlive.connections'));
});
Breadcrumbs::register('live-cirqlive-connections-all', function ($breadcrumbs) {
    $breadcrumbs->parent('live-cirqlive-connections');
    $breadcrumbs->push('All Connections', route('cirqlive.connections.all'));
});
Breadcrumbs::register('live-cirqlive-connections-show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('live-cirqlive-connections-all');
    $breadcrumbs->push('Connection info: ' . $id, route('cirqlive.connections.show', ['id' => $id]));
});

// USERS
Breadcrumbs::register('live-cirqlive-courses', function ($breadcrumbs) {
    $breadcrumbs->parent('live-cirqlive');
    $breadcrumbs->push('Courses', route('cirqlive.courses'));
});
Breadcrumbs::register('live-cirqlive-courses-all', function ($breadcrumbs) {
    $breadcrumbs->parent('live-cirqlive-courses');
    $breadcrumbs->push('All Courses', route('cirqlive.courses.all'));
});
Breadcrumbs::register('live-cirqlive-courses-show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('live-cirqlive-courses');
    $breadcrumbs->push('Course info: ' . $id, route('cirqlive.courses.show', ['id' => $id]));
});
<?php

return [

    // URLs

    'api_host' => env('CIRQLIVE_API_HOST'),

    // CREDENTIALS

    'username' => env('CIRQLIVE_API_USERNAME'),
    'password' => env('CIRQLIVE_API_PASSWORD'),
    'auth_method' => env('CIRQLIVE_API_AUTH_METHOD'),

    // SETTINGS

    'notification_mode' => ['flash', 'log'], // flash, log, or empty array for none

    'debug_mode' => false, // true - extra logging in Barryvdh Debugbar (required)

    // CACHE

    'cache_active' => env('CIRQLIVE_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes' => env('CIRQLIVE_API_CACHE_MINUTES', 10),
    'cache_endpoints' => [

        // for now the cirqlive function names
        'list_connections',
        'list_courses',
        'list_conferencing_events',
        'list_conference_recordings',
        'get_conferencing_event_attendance',
    ],

];
<?php

namespace App;

use Uncgits\CcpsApiLog\Events\ApiCallAttempted;
use Uncgits\WebexApiLaravel\WebexApi as BaseWebexApi;
use App\CcpsCore\DbConfig;

class WebexApi extends BaseWebexApi
{

    /**
     * WebexApi constructor.
     *
     * wrapper around the parent wrapper class so that we can ensure that the debugbar is active before trying to use it.
     */
    public function __construct($environment = null)
    {
        parent::__construct();

        if (DbConfig::getConfig('debugbar') != 'enabled') {
            $this->debugMode = false;
        }

        // environment detection

        if (is_null($environment)) {
            $environment = $this->getEnvironment();
        }

        $definedEnvironments = config('webex-viewer.environments');
        if (!isset($definedEnvironments[$environment])) {
            throw new \Exception('Webex API Environment "' . $environment . '" is not registered in the configuration.');
        }

        // re-set creds based on environment
        $this->setXmlHost(config('webex-viewer.environments.' . $environment . '.xml_host'));
        $this->setNbrHost(config('webex-viewer.environments.' . $environment . '.nbr_host'));
        $this->setUsername(config('webex-viewer.environments.' . $environment . '.username'));
        $this->setPassword(config('webex-viewer.environments.' . $environment . '.password'));
        $this->setSiteName(config('webex-viewer.environments.' . $environment . '.site_name'));
        $this->setPartnerId(config('webex-viewer.environments.' . $environment . '.partner_id'));

        // add environment name as prepend to cache keys
        $this->setHashedCacheKeyPrepend($environment);
    }

    public function getEnvironment()
    {
        $environment = data_get(app('dbConfig'), 'environment', null);

        if (is_null($environment)) {
            $environment = config('webex-viewer.default_environment');
        }

        return $environment;
    }


    /**
     * xmlApiCall() - wrapper around the parent wrapper class so that we can log the call in our app database.
     *
     * @param string $service
     * @param string $request
     * @param string $bodyContent
     * @param null $asUser
     *
     * @return array
     */
    public function xmlApiCall($service, $request, $bodyContent = '', $asUser = null)
    {

        $result = parent::xmlApiCall($service, $request, $bodyContent, $asUser);

        if ($result['source'] == 'api') {
            event(new ApiCallAttempted($result, 'webex'));
        }

        return $result;
    }
}
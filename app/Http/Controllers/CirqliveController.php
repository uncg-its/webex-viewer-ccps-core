<?php

namespace App\Http\Controllers;

use App\CirqliveApi;
use Uncgits\Ccps\Support\CcpsPaginator;
use Illuminate\Http\Request;

class CirqliveController extends Controller
{
    protected $cirqliveApi;

    public function __construct() {
        $this->cirqliveApi = new CirqliveApi();

        $this->middleware('permission:cirqlive.view');
    }

    // helper methods
    public function getConnectionsList() {
        $request = $this->cirqliveApi->listConnections();

        $connections = collect($request['body']->connections)->keyBy('meetsIdConnection');

        return $connections;
    }

    public function getCoursesList() {
        $request = $this->cirqliveApi->listCourses();

        $courses = collect($request['body']->courses)->keyBy('meetsIdCourse');

        return $courses;
    }


    // ------------------------

    public function index() {
        return view('live.cirqlive.index');
    }

    public function connectionsIndex() {
        return view('live.cirqlive.connections.index');
    }

    public function connectionsAll() {
        $connections = $this->getConnectionsList();

        return view('live.cirqlive.connections.all')->with([
            'connections' => new CcpsPaginator($connections)
        ]);
    }

    public function connectionsShow($id) {
        $connections = $this->getConnectionsList();

        $connection = isset($connections[$id]) ? $connections[$id] : false;

        return view('live.cirqlive.connections.show')->with(compact(['connection', 'id']));
    }

    public function coursesIndex() {
        return view('live.cirqlive.courses.index');
    }

    public function coursesAll() {
        $courses = $this->getCoursesList();

        return view('live.cirqlive.courses.all')->with([
            'courses' => new CcpsPaginator($courses)
        ]);
    }

    public function coursesShow($id) {
        $courses = $this->getCoursesList();
        $connections = $this->getConnectionsList();

        $course = isset($courses[$id]) ? $courses[$id] : false;

        if ($course) {
            $connection = isset($connections[$course->meetsIdConnection]) ? $connections[$course->meetsIdConnection] : false;
        }

        return view('live.cirqlive.courses.show')->with(compact(['course', 'connection', 'id']));
    }
}

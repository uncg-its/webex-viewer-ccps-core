<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Uncgits\Ccps\Support\CcpsPaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\WebexApi;

class WebexController extends Controller
{
    // properties
    protected $webexApi;

    protected $siteKeys = [
        'use:meetingCenter'  => 'Meeting Center',
        'use:trainingCenter' => 'Training Center',
        'use:eventCenter'    => 'Event Center',
        'use:supportCenter'  => 'Support Center'
    ];

    protected $historyKeys = [
        'Meeting Center' => 'history:meetingUsageHistory'
    ];

    // helper method
    protected function getUserList($type = 'all', $paginator = true)
    {
        switch ($type) {
            case 'active':
                $call = 'getActiveUsers';
                break;
            case 'all':
            default:
                $call = 'getAllUsers';
                break;
        }

        $request = $this->webexApi->$call();

        $users = collect($request['data'])->keyBy('use:webExId');

        return $paginator ? new CcpsPaginator($users->sort()) : $users->sort();
    }

    /*
    |--------------------------------------------------------------------------
    | Constructor
    |--------------------------------------------------------------------------
    |
    | Instantiate Webex API object, set middleware
    |
    */

    public function __construct()
    {
        // instantiate the API interface
        // Webex API model will be used in most methods in this controller.
        $this->webexApi = new WebexApi();

        $this->middleware('permission:webex.view')->except(['index']);
    }

    public function index()
    {
        return view('live.webex.index');
    }

    /*
    |--------------------------------------------------------------------------
    | Site
    |--------------------------------------------------------------------------
    */

    public function siteIndex()
    {
        return view('live.webex.site.index');
    }

    public function siteInfo()
    {
        $site = $this->webexApi->getSiteInfo();

        return view('live.webex.site.info')->with(compact('site'));
    }

    public function siteXmlApiVersion()
    {
        $api = $this->webexApi->getXmlApiVersion();
        return view('live.webex.site.xml-api')->with(compact('api'));
    }

    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    */

    public function usersIndex()
    {
        return view('live.webex.users.index');
    }

    public function usersAll()
    {
        $paginator = $this->getUserList('all');
        return view('live.webex.users.all')->with(['users' => $paginator]);
    }

    public function usersActive()
    {
        $paginator = $this->getUserList('active');
        return view('live.webex.users.active')->with(['users' => $paginator]);
    }

    public function usersShow($id)
    {
        $user = $this->webexApi->getUser($id);

        return view('live.webex.users.show')->with([
            'webExUser' => $user['data'],
            'siteKeys'  => $this->siteKeys
        ]);
    }

    public function usersSearch(Request $request)
    {
        if (isset($request->search)) {
            $acceptableCriteria = ['webExId', 'firstName', 'lastName', 'email'];

            if (!in_array($request->criterion, $acceptableCriteria)) {
                flash('Error: invalid search criterion specified.', 'danger');
                return Redirect::back();
            }

            $users = collect($this->getUserList('all', false))->keyBy('use:webExId');

            // build preg_match
            switch ($request->operand) {
                case 'contains':
                    $regex = '/' . $request->search . '/';
                    break;
                case 'begins':
                    $regex = '/\\A' . $request->search . '/';
                    break;
                case 'ends':
                    $regex = '/' . $request->search . '\\Z/';
                    break;
                case 'exact':
                default:
                    $regex = '/\\A' . $request->search . '\\Z/';
                    break;
            }

            switch ($request->active) {
                case 'active':
                    $statuses = ['ACTIVATED'];
                    break;
                case 'inactive':
                    $statuses = ['DEACTIVATED'];
                    break;
                case 'any':
                default:
                    $statuses = ['ACTIVATED', 'DEACTIVATED'];
                    break;
            }

            $matches = $users->filter(function ($item) use ($regex, $request, $statuses) {
                if (preg_match($regex, $item['use:' . $request->criterion]) == 1) {
                    return in_array($item['use:active'], $statuses);
                };

                return false;
            });

            return view('live.webex.users.search')->with([
                'results'   => new CcpsPaginator($matches),
                'status'    => $request->status,
                'criterion' => $request->criterion,
                'operand'   => $request->operand,
                'search'    => $request->search
            ]);
        }

        return view('live.webex.users.search');
    }

    /*
    |--------------------------------------------------------------------------
    | Recordings
    |--------------------------------------------------------------------------
    */

    public function recordingsIndex()
    {
        return view('live.webex.recordings.index');
    }

    public function recordingsAll()
    {
        $request = $this->webexApi->getRecordings();

        if (isset($request['data']['ep:recording'])) {
            $recordings = collect($request['data']['ep:recording'])->keyBy('ep:recordingID')->sortByDesc(function ($item
            ) {
                return Carbon::createFromFormat('m/d/Y H:i:s', $item['ep:createTime'])->toDateTimeString();
            });
        } else {
            $recordings = collect([]);
        }

        return view('live.webex.recordings.all')->with([
            'recordings'  => new CcpsPaginator($recordings),
            'historyDays' => config('webex-api.history_days') <= 28 ? config('webex-api.history_days') : 28
        ]);
    }

    public function recordingsSearch(Request $request)
    {
        if (isset($request->search)) {
            $acceptableCriteria = ['recordingID', 'hostWebExID', 'confID'];

            if (!in_array($request->criterion, $acceptableCriteria)) {
                flash('Error: invalid search criterion specified.', 'danger');
                return Redirect::back();
            }

            // get recording data from API
            $recordings = $this->webexApi->getRecordings();

            // collection
            if (count($recordings['data']) > 0) {
                $recordingCollection = collect($recordings['data']['ep:recording'])->keyBy('ep:recordingID')->sortByDesc(function (
                    $item
                ) {
                    return Carbon::createFromFormat('m/d/Y H:i:s', $item['ep:createTime'])->toDateTimeString();
                });
            } else {
                $recordingCollection = collect([]);
            }

            // build preg_match
            switch ($request->operand) {
                case 'contains':
                    $regex = '/' . $request->search . '/';
                    break;
                case 'begins':
                    $regex = '/\\A' . $request->search . '/';
                    break;
                case 'ends':
                    $regex = '/' . $request->search . '\\Z/';
                    break;
                case 'exact':
                default:
                    $regex = '/\\A' . $request->search . '\\Z/';
                    break;
            }

            $matches = $recordingCollection->filter(function ($item) use ($regex, $request) {
                return preg_match($regex, $item['ep:' . $request->criterion]) == 1;
            });

            return view('live.webex.recordings.search')->with([
                'results'     => new CcpsPaginator($matches),
                'criterion'   => $request->criterion,
                'operand'     => $request->operand,
                'search'      => $request->search,
                'historyDays' => config('webex-api.history_days') <= 28 ? config('webex-api.history_days') : 28
            ]);
        }

        return view('live.webex.recordings.search')->with([
            'historyDays' => config('webex-api.history_days') <= 28 ? config('webex-api.history_days') : 28
        ]);
    }

    public function recordingsShow($id)
    {
        $recording = $this->webexApi->getRecording($id);

        return view('live.webex.recordings.show')->with([
            'recording' => $recording['data']['ep:recording'],
            'id'        => $id
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Sessions
    |--------------------------------------------------------------------------
    */

    public function sessionsIndex()
    {
        return view('live.webex.sessions.index');
    }

    public function sessionsAll()
    {
        $sessions = $this->webexApi->getPastSessions();

        $paginator = new CcpsPaginator(collect($sessions['data'])->keyBy('history:confID')->sortByDesc(function ($item
        ) {
            return Carbon::createFromFormat('m/d/Y H:i:s', $item['history:sessionStartTime'])->toDateTimeString();
        }));

        return view('live.webex.sessions.all')->with([
            'sessions'    => $paginator,
            'historyDays' => config('webex-api.history_days')
        ]);
    }

    public function sessionsScheduled()
    {
        $sessions = $this->webexApi->getScheduledSessions();

        $paginator = new CcpsPaginator(collect($sessions['data'])->keyBy('sessionKey')->sortBy(function ($item) {
            return Carbon::createFromFormat('m/d/Y H:i:s', $item['startDate'])->toDateTimeString();
        }));

        return view('live.webex.sessions.scheduled')->with([
            'sessions' => $paginator
        ]);
    }

    public function sessionsInProgress()
    {
        $sessions = $this->webexApi->getSessionsInProgress();

        $paginator = new CcpsPaginator(collect($sessions['data'])->keyBy('ep:sessionKey'));

        return view('live.webex.sessions.in-progress')->with(['sessions' => $paginator]);
    }

    public function sessionsSearch(Request $request)
    {
        if (isset($request->search)) {
            $acceptableCriteria = ['confID', 'hostWebExID', 'confName'];

            if (!in_array($request->criterion, $acceptableCriteria)) {
                flash('Error: invalid search criterion specified.', 'danger');
                return Redirect::back();
            }

            // get session data from API
            $sessions = $this->webexApi->getPastSessions();

            // collection
            $sessionCollection = collect($sessions['data'])->keyBy('history:confID')->sortByDesc(function ($item) {
                return Carbon::createFromFormat('m/d/Y H:i:s', $item['history:sessionStartTime'])->toDateTimeString();
            });

            // build preg_match
            switch ($request->operand) {
                case 'contains':
                    $regex = '/' . $request->search . '/';
                    break;
                case 'begins':
                    $regex = '/\\A' . $request->search . '/';
                    break;
                case 'ends':
                    $regex = '/' . $request->search . '\\Z/';
                    break;
                case 'exact':
                default:
                    $regex = '/\\A' . $request->search . '\\Z/';
                    break;
            }

            $matches = $sessionCollection->filter(function ($item) use ($regex, $request) {
                return preg_match($regex, $item['history:' . $request->criterion]) == 1;
            });

            return view('live.webex.sessions.search')->with([
                'results'     => new CcpsPaginator($matches),
                'criterion'   => $request->criterion,
                'operand'     => $request->operand,
                'search'      => $request->search,
                'historyDays' => config('webex-api.history_days')
            ]);
        }

        return view('live.webex.sessions.search')->with([
            'historyDays' => config('webex-api.history_days')
        ]);
    }

    public function sessionsShow($id)
    {
        $session = $this->webexApi->getPastSession($id);

        return view('live.webex.sessions.show')->with([
            'session' => $session['data'],
            'confId'  => $id,
            'center'  => $session['center']
        ]);
    }

    public function sessionsScheduledShow($id)
    {
        dd("this is not currently possible.");
        $session = $this->webexApi->getScheduledSession($id);

        dd($session);

        return view('live.webex.sessions.showScheduled')->with([
            'session'    => $session['data'],
            'sessionKey' => $id,
            'center'     => $session['center']
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Uncgits\WebexApi\WebexApi;

class TestController extends Controller
{
    public function index() {
//        $webexApi = new WebexApi();
//        dd($webexApi->getUserStatus('mslibera'));

        return "testing 123";
    }

    public function proxy() {
        $client = new Client();
//        $response = $client->get('http://httpbin.org/get', ['proxy' => '92.110.45.226:80']);
        $response = $client->get('http://httpbin.org/get');

        $code = $response->getStatusCode();
        $reason = $response->getReasonPhrase();
        $body = $response->getBody();
        $bodyContents = $body->getContents(); // parse XML to array

        dd($code, $reason, $bodyContents);
    }
}

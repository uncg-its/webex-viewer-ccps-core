<?php

namespace App\Http\Controllers;

class LiveController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Constructor
    |--------------------------------------------------------------------------
    |
    | Instantiate Webex API object, set middleware
    |
    */

    public function __construct() {

        $this->middleware('permission:webex.view|cirqlive.view');

    }

    public function index() {
        return view('live.index');
    }

}


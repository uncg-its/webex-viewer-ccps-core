<?php

namespace App\Http\Controllers\CcpsCore;

use App\Http\Requests\CcpsCore\ConfigUpdateRequest;
use Uncgits\Ccps\Controllers\DbConfigController as BaseController;

class DbConfigController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // construct environment key=>display array for form
        $selectArray = [];

        foreach (config('webex-viewer.environments') as $key => $data) {
            $selectArray[$key] = $data['display_name'];
        }

        return view($this->viewPath . 'config.index')->with([
            'dbConfig'     => app('dbConfig'),
            'environments' => $selectArray,
        ]);
    }
}

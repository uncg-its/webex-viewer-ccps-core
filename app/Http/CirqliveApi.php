<?php

namespace App;

use Uncgits\CcpsApiLog\Events\ApiCallAttempted;
use Uncgits\CirqliveApiLaravel\CirqliveApi as BaseCirqliveApi;
use App\CcpsCore\DbConfig;

class CirqliveApi extends BaseCirqliveApi {

    /**
     * WebexApi constructor.
     *
     * wrapper around the parent wrapper class so that we can ensure that the debugbar is active before trying to use it.
     */
    public function __construct() {
        parent::__construct();

        if (DbConfig::getConfig('debugbar') != 'enabled') {
            $this->debugMode = false;
        }

    }


    /**
     * apiCall() - wrapper around the parent wrapper class so that we can log the call in our app database.
     *
     * @param string $function
     * @param string $method
     * @param array $params
     * @param boolean $download
     *
     * @return array
     */
    public function apiCall($function, $method, $params = [], $download = false) {

        $result = parent::apiCall($function, $method, $params, $download);

        if ($result['source'] == 'api') {
            event(new ApiCallAttempted($result, 'cirqlive'));
        }

        return $result;
    }
}
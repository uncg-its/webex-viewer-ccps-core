# Webex Viewer application

## What is this?

This is an application that provides a GUI around Webex APIs. This application is designed to provide quick access to data from the Webex and CirQlive platforms in a view-only sense.

## Who maintains this?

This application was built and is maintained by the UNCG CCPS Developers Group:  ccps-developers-l@uncg.edu

---

# Installation

## Required dependencies

You will need to paste the following into your `composer.json` file in order to access repositories not hosted in Packagist:

```
"repositories": [
    {
        "type": "git",
        "url": "https://bitbucket.org/uncg-its/ccps-core.git"
    },
    {
        "type": "git",
        "url": "https://bitbucket.org/uncg-its/cron.git"
    },
    {
        "type": "git",
        "url": "https://bitbucket.org/uncg-its/loggy.git"
    }
],
```
## Deploying the App

1. Clone this repository
2. Add the `repositories` code above to your `composer.json` file
3. Run `composer install`
4. Run `php artisan ccps:deploy` to handle database migrations and seeding, creation of the `.env` file, etc.
5. Enter data into your `.env` file around the Webex and CirQlive APIs, mail / log settings, etc.
6. Configure your server for proper web access to the `/public` folder of this application (per Laravel's typical install process)
7. Visit your app's URL and log in with the username `admin@admin.com`, and the password you set in the `ccps:deploy` command.
8. Configure new user(s) and remove the default user.

---

# Usage

All of the API operations should be fairly self-explanatory.

## Permissions

By default the "admin" role will have complete access to the application. There are also two "viewer" roles that can be assigned to other users as appropriate. One permission is assigned to each role by default (separating out Webex API from CirQlive API).

## Caching

Most API calls are cached. You can visit the `config/webex-api.php` and `config/cirqlive-api.php` files to change some settings. By default, the API Cache will be active on a 10-minute window, meaning that the app will not make a new API call if the same one has been made in the last 10 minutes. This is in an effort to reduce traffic load on the Webex and CirQlive APIs, and can be changed or disabled based on how heavy you anticipate the usage of this app to be.

 ## Statistics
 
 The app tracks statistics on API calls, using the `uncgits/ccps-api-logging-stats` package.

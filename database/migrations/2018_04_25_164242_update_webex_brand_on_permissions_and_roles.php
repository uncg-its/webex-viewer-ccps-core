<?php

use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWebexBrandOnPermissionsAndRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {
            $role = Role::where('name', 'viewer-webex')->firstOrFail();
            $role->update([
                'display_name' => str_replace('WebEx', 'Webex', $role->display_name),
                'description' => str_replace('WebEx', 'Webex', $role->description),
            ]);

            $permission = Permission::where('name', 'webex.view')->firstOrFail();
            $permission->update([
                'display_name' => str_replace('WebEx', 'Webex', $permission->display_name),
                'description' => str_replace('WebEx', 'Webex', $permission->description),
            ]);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            $role = Role::where('name', 'viewer-webex')->firstOrFail();
            $role->update([
                'display_name' => str_replace('Webex', 'WebEx', $role->display_name),
                'description' => str_replace('Webex', 'WebEx', $role->description),
            ]);

            $permission = Permission::where('name', 'webex.view')->firstOrFail();
            $permission->update([
                'display_name' => str_replace('Webex', 'WebEx', $permission->display_name),
                'description' => str_replace('Webex', 'WebEx', $permission->description),
            ]);
        });
    }
}
